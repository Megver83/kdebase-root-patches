### Patches for Kate and Dolphin to be run as root.

* Upstream: [OpenSUSE](http://download.opensuse.org/repositories/KDE:/Applications/openSUSE_Factory_standard/src/)

If there are more patches to be added here, ask for them in the issue tracker with the [REQUEST] tag.

### Ready-to-use patched programs

#### Arch Linux (AUR)
* [kate-root & kwrite-root](https://aur.archlinux.org/pkgbase/kate-root/)
* [dolphin-root](https://aur.archlinux.org/packages/dolphin-root/)

#### Parabola GNU/Linux-libre

##### x86_64: 
* [kate-root](https://www.parabola.nu/packages/pcr/x86_64/kate-root/)
* [kwrite-root](https://www.parabola.nu/packages/pcr/x86_64/kwrite-root/)
* [dolphin-root](https://www.parabola.nu/packages/pcr/x86_64/dolphin-root/)

##### i686: 
* [kate-root](https://www.parabola.nu/packages/pcr/i686/kate-root/)
* [kwrite-root](https://www.parabola.nu/packages/pcr/i686/kwrite-root/)
* [dolphin-root](https://www.parabola.nu/packages/pcr/i686/dolphin-root/)

##### armv7h:
* [kate-root](https://www.parabola.nu/packages/pcr/armv7h/kate-root/)
* [kwrite-root](https://www.parabola.nu/packages/pcr/armv7h/kwrite-root/)
* [dolphin-root](https://www.parabola.nu/packages/pcr/armv7h/dolphin-root/)
